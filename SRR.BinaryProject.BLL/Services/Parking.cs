﻿using SRR.BinaryProject.Shared;
using SRR.BinaryProject.Shared.Interfaces;
using SRR.BinaryProject.Shared.Models;
using SRR.BinaryProject.Shared.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SRR.BinaryProject.BLL.Services
{
    public class Parking : IParking
    {
        private Parking() {
            timerToCharge = new Timer(ParkingConfiguration.PeriodOfCharging * 1000);
            timerToCharge.Elapsed += OnTimeToCharge;
            timerToCharge.Start();
            timerToWriteLogs = new Timer( 60 * 1000);
            timerToWriteLogs.Elapsed += OnTimeToWriteLogsToFile;
            timerToWriteLogs.Start();

        }
        public static Parking Intsance => Nested.instance;
        private class Nested
        {
            static Nested() { }
            internal static readonly Parking instance = new Parking();
        }

        public double BalanceOfParking { get; private set; }
        public  int NumberOfLots => ParkingConfiguration.ParkingCapacity;
        private List<ITransportItem> TransportItems { get; } = new List<ITransportItem>();
        public IEnumerable<ITransportItem> ParkedTransport => TransportItems;
        private List<Transaction> LastTransactionsRegister { get; } = new List<Transaction>();
        public IEnumerable<Transaction> LastTransactions => LastTransactionsRegister;
        public int AvailableLotsNumber => NumberOfLots - TransportItems.Count;
        public IReadOnlyDictionary<TransportTypes, double> Tariffs => ParkingConfiguration.PriceForParking;
        public double ParkingFine => ParkingConfiguration.FineCoef;
        private readonly Timer timerToCharge;
        private readonly Timer timerToWriteLogs;

        private ILogger LogWriter { get; } = Logger.Intstance;

        private void OnTimeToCharge(object source, ElapsedEventArgs e)
        {
            GetPaymentsFromTransports();
        }

        private void OnTimeToWriteLogsToFile(object source, ElapsedEventArgs e)
        {
            if (LastTransactionsRegister.Count == 0)
            {
                return;
            }
            LogWriter.WriteLogs(LastTransactions.ToArray());
            LastTransactionsRegister.Clear();
        }

        public void GetPaymentsFromTransports()
        {
            foreach (var transportItem in TransportItems)
            {
               Transaction transaction = transportItem.PayForParking(this);
                if (transaction == null)
                {
                    continue;
                }
                BalanceOfParking += transaction.TransactionSum;
                LastTransactionsRegister.Add(transaction);
            }
        }

        public bool PutTransportInParking(ITransportItem transportItem)
        {
            if (AvailableLotsNumber == 0)
            {
                return false;
            }
            TransportItems.Add(transportItem);
            return true;
        }

        public bool RemoveTransportFromParking(int transportNumber)
        {
            if (transportNumber < TransportItems.Count && TransportItems.Count != 0)
            {
                TransportItems.RemoveAt(transportNumber);
                return true;
            }
            return false;
        }
    }
}
