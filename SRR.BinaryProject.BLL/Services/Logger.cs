﻿using SRR.BinaryProject.Shared.Interfaces;
using SRR.BinaryProject.Shared.Models;
using SRR.BinaryProject.Shared.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRR.BinaryProject.BLL.Services
{
    public class Logger : ILogger
    {
        private readonly string Path;
        private Logger()
        {
            Path = ParkingConfiguration.LogPath;
            FileInfo fi = new FileInfo(Path);
            if (fi.Exists)
            {
                fi.Delete();
            }
            using (var fileStream = new FileStream(Path, FileMode.Create)) { }
        }
        public static Logger Intstance => Nested.instance;
        private class Nested
        {
            static Nested() { }
            internal static readonly Logger instance = new Logger();
        }

        public void WriteLogs(IEnumerable<Transaction> transactions)
        {
            using (var fileStream = new FileStream(Path, FileMode.Append))
            using (var streamWriter = new StreamWriter(fileStream))
            {
                foreach (var transaction in transactions)
                {
                    streamWriter.WriteLine($"{DateTime.Now} {transaction}");
                }
                streamWriter.WriteLine();
                
            }
        }
    }
}
