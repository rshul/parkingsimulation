﻿using SRR.BinaryProject.Shared;
using SRR.BinaryProject.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRR.BinaryProject.BLL.Services
{
    public class UserInteraction : IUserInteraction
    {
        private IParking Parking { get; set; }
        public double BalanceOfParking => Parking.BalanceOfParking;
        public double LastMinuteIncome => Parking.LastTransactions.Select(x => x.TransactionSum).Sum();
        public int FreeLotsNumber => Parking.AvailableLotsNumber;
        public int BusyLotsNumber => Parking.NumberOfLots - Parking.AvailableLotsNumber;
        public IEnumerable<ITransportItem> ParkedTransport => Parking.ParkedTransport;
        private readonly IReader reader;

        public UserInteraction(IParking parking, IReader reader)
        {
            Parking = parking;
            this.reader = reader;
        }

        public void OutputLastMinuteTransactions()
        {
            foreach (var transaction in Parking.LastTransactions)
            {
                Console.WriteLine(transaction);
            }
        }

        public void OutputTransactionFromFile()
        {
            IEnumerable<string> lines = reader.ReadLogs();
            if (lines == null)
            {
                return;
            }
            foreach (var line in lines)
            {
                Console.WriteLine(line);
            }
        }

        public bool PutTransportInParking(TransportTypes transportType)
        {
            return Parking.PutTransportInParking(new TransportItem(transportType));
        }

        public bool RemoveTransportFromParking(int transportNumber)
        {
            return Parking.RemoveTransportFromParking(transportNumber);
        }

        public bool TopUpTransportAccount(Guid transportID, double topUpSum)
        {
            if (topUpSum < 0 || topUpSum > 1000000)
            {
                return false;
            }
            var TransportItem = ParkedTransport.FirstOrDefault(i => i.TransportItemID == transportID);
            if (TransportItem == null)
            {
                return false;
            }
            TransportItem.TopUpAccount(topUpSum);
            return true;
        }
    }
}
