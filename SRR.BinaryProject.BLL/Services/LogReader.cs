﻿using SRR.BinaryProject.Shared.Interfaces;
using SRR.BinaryProject.Shared.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRR.BinaryProject.BLL.Services
{
    public class LogReader : IReader
    {
        private LogReader() { }
        public static LogReader Intstance => Nested.instance;
        private class Nested
        {
            static Nested() { }
            internal static readonly LogReader instance = new LogReader();
        }
        private readonly string Path = ParkingConfiguration.LogPath;

        public IEnumerable<string> ReadLogs()
        {
            List<string> outPutResult = new List<string>();
            try
            {

                using (var reader = new StreamReader(Path))
                {
                    while (!reader.EndOfStream)
                    {
                        outPutResult.Add(reader.ReadLine());
                    }
                }
            }
            catch(FileNotFoundException exception)
            {
                Console.WriteLine(exception.Message);
                return null;
            }catch(Exception exception)
            {
                Console.WriteLine(exception.Message);
                return null;
            }

            return outPutResult;
        }
    }
}
