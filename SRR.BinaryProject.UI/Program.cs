﻿using SRR.BinaryProject.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRR.BinaryProject.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            MenuCreator.CommandsProvider = new UserInteraction(Parking.Intsance, LogReader.Intstance );
            MenuCreator.MainMenu();
        }
    }
}
