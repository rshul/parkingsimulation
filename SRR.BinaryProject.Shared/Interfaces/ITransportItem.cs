﻿using SRR.BinaryProject.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRR.BinaryProject.Shared.Interfaces
{
    public interface ITransportItem
    {
        Guid TransportItemID { get; }
        double Balance { get;  }
        Transaction PayForParking(IParking parking);
        void TopUpAccount(double acceptedSum);
    }
}
