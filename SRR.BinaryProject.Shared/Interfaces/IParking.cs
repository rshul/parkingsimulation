﻿using SRR.BinaryProject.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRR.BinaryProject.Shared.Interfaces
{
    public interface IParking
    {
        double BalanceOfParking { get; }
        int NumberOfLots { get; }
        IEnumerable<ITransportItem> ParkedTransport { get; }
        IEnumerable<Transaction> LastTransactions { get; }
        int AvailableLotsNumber { get; }
        bool PutTransportInParking(ITransportItem transportItem);
        bool RemoveTransportFromParking(int transportNumber);
        void GetPaymentsFromTransports();
        IReadOnlyDictionary<TransportTypes, double> Tariffs { get; }
        double ParkingFine { get; }
    }
}
