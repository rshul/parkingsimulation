﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRR.BinaryProject.Shared.Interfaces
{
    public interface IUserInteraction
    {
        double BalanceOfParking { get;}
        double LastMinuteIncome { get; }
        int FreeLotsNumber { get; }
        int BusyLotsNumber { get; }
        IEnumerable<ITransportItem> ParkedTransport { get; }
        void OutputLastMinuteTransactions();
        void OutputTransactionFromFile();
        bool PutTransportInParking(TransportTypes transportType);
        bool RemoveTransportFromParking(int transportNumber);
        bool TopUpTransportAccount(Guid transportID, double topUpSum);

    }
}
